# Job Accessibility

A repo to create and store job accessibility maps. Created with data from The Accessibility Observatory at the University of Minnesota.

## Introduction

Measuring the performance of transportation systems as a whole isn't an easy task. To understand why modal splits between private cars and public transportation fall the way they do across cities, it might be helpful to understand which mode provides better access to jobs or other amenities within a given time. In some places public transit is extremely efficent at providing access to jobs, while in other places, transit access is very poor.

By measuring the ratio of jobs accessible by car to jobs accessible by transit within a given commute time, we can effectively map what areas of cities are served competitively, and which are completely auto dependant. 

It's rare for cities to have areas where transit provides better access to jobs than a private car, so the maps' scale is set such that blue is where job access is equal. Dark red areas have 10 times more jobs accessible within the given commute time than via transit.

Seatle is somewhat of an outlier in that it has a few areas, served by ferries, which have better transit access than car access, but this is uncommon.

![Transit accessibility ratio for Seatle](<./figures/Seatle/Seatle_transit_accessibility_ratio_60_minutes_parity.png>) 

Even old cities with relatively good rail service have a hard time reaching parity with auto accessiblity.

![Transit accessibility ratio for Boston](<./figures/Boston/Boston_transit_accessibility_ratio_60_minutes_parity.png>) 

Newer cities barely have any blue tinted regions, places where commuting transit provides access to at least 1/3 the jobs as by car.
![Transit accessibility ratio for Nashville](<./figures/Nashville/Nashville_transit_accessibility_ratio_60_minutes_parity.png>) 

### Data and Reproducibility

Data for these maps was collected by The Accessibility Observatory at the University of Minnesota http://ao.umn.edu

I specifically used the 2018 Auto and Transit "Access Across America" datasets availible here: https://conservancy.umn.edu/handle/11299/200592

To reproduce these maps, you'll need to download the data from the above link, and run `run_all.sh` with the necessary python environment. The most important dependency is getting Geopandas installed. If you actually want to reproduce, send an email, and I'll make some better documentation, and upload a requirements.txt to build a conda environment.

